## MICROSERVICES USING SPRINGBOOT

- Created service registry (to manage the services).
- Created user service (to manage the information of user).
- Created hotel service (to manage and maintain the data of hotels).
- Created rating service (to manage the rating of hotel).
- Created config server (to get the config from server(gitlab) realtime).
- Created api gateway (to get all the services from one end point).

1.  worked on discovery server and client.
2.  microservices communication
3.  Feign client (to get data from another service)
4.  Api gateway
5.  Config server
6.  Fault tolerance
7.  Retry module
8.  Rate module
9.  Spring security
10. Spring security with OKTA

## LOCAL SETUP
- setup java 17 (for all the services)
- Enable database servers of postgres, mysql and mongodb.
- Create database for all three services (UserService, HotelService and RatingSerive) according to the application.yml respectively.
- Run the main file of the services in the same order provided below:
  1. ServiceRegistry
  2. Config Service (Here I have added my config from my private git (edit accordingly for your project), you can refer application-dev.yml in repo root directory)
  3. RatingSerive
  4. HotelService
  5. UserService
  6. ApiGateway
