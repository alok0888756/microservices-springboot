package org.npci.userservice.controller;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import org.npci.userservice.entities.User;
import org.npci.userservice.services.UserService;
import org.npci.userservice.services.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    // create
    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user){
        User user1 = userService.saveUser(user);
        return  ResponseEntity.status(HttpStatus.CREATED).body(user1);
    }

    int retryCount = 1;
    // single user get
    @GetMapping("/{userId}")
    @CircuitBreaker(name = "ratingHotelBreaker", fallbackMethod = "ratingHotelFallback")
    @Retry(name = "ratingHotelBreaker", fallbackMethod = "ratingHotelFallback")
    public ResponseEntity<User> getSingleUser(@PathVariable String userId){
        logger.info("Get single user handler: UserController");
        logger.info("Retry count: {}", retryCount);
        retryCount++;
        User user1 = userService.getUser(userId);
        return ResponseEntity.ok(user1);
    }

    // creating fallback method for circuit breaker (this method will run when user or rating service is down)
    public ResponseEntity<User> ratingHotelFallback(String userId, Exception ex){
        //logger.info("Fallback is executed because service is down: " ,ex.getMessage());

        User user = new User("1234","Dummy", "dammy@gmail.com", "dummy developer");
        return new ResponseEntity<>(user,HttpStatus.OK);
    }
    // get all users
    @GetMapping
    public  ResponseEntity<List<User>> getAllUser(){
        List<User> allUser = userService.getAllUser();
        return ResponseEntity.ok(allUser);
    }

}
