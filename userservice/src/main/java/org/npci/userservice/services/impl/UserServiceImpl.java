package org.npci.userservice.services.impl;

import org.npci.userservice.entities.Hotel;
import org.npci.userservice.entities.Rating;
import org.npci.userservice.entities.User;
import org.npci.userservice.exception.ResourceNotFoundException;
import org.npci.userservice.external.services.HotelService;
import org.npci.userservice.repository.UserRepository;
import org.npci.userservice.services.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HotelService hotelService;

    private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public User saveUser(User user) {
        // generate unique id
        String randomUserId = UUID.randomUUID().toString();
        user.setUserId(randomUserId);
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();

    }

    @Override
    public User getUser(String userId) {
        // get user from database with the help of database
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User with given id is not found on given server: "+ userId));

        // fetch rating of above users from rating service
        // http://localhost:9093/ratings/users/01e55c22-3d8c-400d-850b-ad7cd27dea29
        Rating[] ratingsOfUser = restTemplate.getForObject("http://RATING-SERVICE/ratings/users/"+user.getUserId(), Rating[].class);
        logger.info("{} ", ratingsOfUser);
        List<Rating> ratings = Arrays.stream(ratingsOfUser).toList();


        List<Rating> ratingList = ratings.stream().map(rating -> {
            // api call to hotel service to get the hotel - http://localhost:9092/hotels/d756f841-4906-4f39-9589-0600bf7fe489
            //ResponseEntity<Hotel> forEntity = restTemplate.getForEntity("http://HOTEL-SERVICE/hotels/"+rating.getHotelId(), Hotel.class);
            // set the hotel to rating
            Hotel hotel = hotelService.getHotel(rating.getHotelId());
           // logger.info("response status code: {} ", forEntity.getStatusCode());
            rating.setHotel(hotel);
            // return the ratings
            return rating;
        }).collect(Collectors.toList());

        user.setRatings(ratingList);
        return user;
    }
}
