package org.npci.userservice.exception;

public class ResourceNotFoundException extends RuntimeException {

    // extra properties you can add
    public ResourceNotFoundException(){
        super("Resource not found on server !!");
    }
    public ResourceNotFoundException(String message){
        super(message);
    }
}
