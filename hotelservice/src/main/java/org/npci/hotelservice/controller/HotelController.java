package org.npci.hotelservice.controller;

import org.npci.hotelservice.entities.Hotel;
import org.npci.hotelservice.services.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hotels")
public class HotelController {

    @Autowired
    private HotelService hotelService;

    //create
    @PostMapping
    public ResponseEntity<Hotel> createHotel (@RequestBody Hotel hotel) {
        return ResponseEntity.status(HttpStatus.CREATED).body(hotelService.create(hotel));
    }


    //get single
    @GetMapping("/{hotelId}")
    public ResponseEntity<Hotel> getHotelById (@PathVariable String hotelId){
        Hotel hotel1 = hotelService.getHotelById(hotelId);
        return ResponseEntity.ok(hotel1);
    }

    //get all
    @GetMapping
    public ResponseEntity<List<Hotel>> getAllHotels (){
        List<Hotel> allHotel =  hotelService.getAll();
        return ResponseEntity.ok(allHotel);
    }
}
