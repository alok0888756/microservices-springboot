package org.npci.hotelservice.services;

import org.npci.hotelservice.entities.Hotel;

import java.util.List;

public interface HotelService {

    // create
    Hotel create (Hotel hotel);

    // get all
    List<Hotel> getAll();

    // get single
    Hotel getHotelById (String id);
}
