package org.npci.hotelservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table (name = "hotels")
public class Hotel {

    @Id
    @Column(name="ID")
    private String hotelId;

    @Column(name="NAME", length = 60)
    private String name;

    @Column(name="LOCATION")
    private String location;

    @Column(name="ABOUT")
    private String about;





    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
