package org.npci.ratingservice.services;

import org.npci.ratingservice.entities.Rating;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RatingService {

    // create
    Rating create (Rating rating);

    // get all ratings
    List<Rating> getRating();

    // user all by userid
    List<Rating> getRatingByUserId(String userId);

    // get all rating by hotelId
    List<Rating> getRatingByHotelId(String hotelId);
}
